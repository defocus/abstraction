﻿
namespace Classes
{
    internal abstract class Document
    {
        public abstract string Content { get; set; }
        public abstract void Show();
    }
}

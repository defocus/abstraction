﻿using System;
using System.Text;

namespace Classes
{
    class Program
    {
        static void Main()
        {
            //Console.OutputEncoding = Encoding.Unicode;

            Document[] doc = { new Title(), new Body(), new Footer() };            
            
            doc[0].Content = "Контракт";
            doc[1].Content = "Тело контракта...";            
            doc[2].Content = "Директор: Иванов И.И.";

            foreach (var document in doc) 
            {
                Console.WriteLine(document.GetType().Name);
                document.Show();
            }

            // Delay.
            Console.ReadKey();
        }
    }
}

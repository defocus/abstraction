﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal abstract class AbstractHandler
    {
        protected abstract void Open();
        protected abstract void Create();
        protected abstract void Chenge();
        protected abstract void Save();
    }
}

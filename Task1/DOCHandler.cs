﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class DOCHandler : AbstractHandler
    {
        public DOCHandler()
        {
            Create();
            Chenge();            
            Save();
            Open();            
        }
        
        protected override void Chenge()
        {
            Console.WriteLine("DOC file is chanched...");
        }

        protected override void Create()
        {
            Console.WriteLine("DOC file is created...");
        }

        protected override void Open()
        {
            Console.WriteLine("DOC file is opened...");
        }

        protected override void Save()
        {
            Console.WriteLine("DOC file is saved...");
        }
    }
}

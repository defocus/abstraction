﻿using System;
using System.IO;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter name file: ");

            string nameFile = Console.ReadLine();

            string type = null;

            for(int i = nameFile.Length - 4; i < nameFile.Length; i++)
            {
                type += nameFile[i];                
            }

            AbstractHandler handler =
                    type.ToLower() switch
                    {
                        ".doc" => new DOCHandler(),
                        ".txt" => new TXTHandler(),
                        ".xml" => new XMLHandler(),
                        _ => null
                    };
                        
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class TXTHandler : AbstractHandler
    {
        public TXTHandler()
        {
            Create();
            Chenge();
            Save();
            Open();
        }
        protected override void Chenge()
        {
            Console.WriteLine("TXT file is chanched...");
        }

        protected override void Create()
        {
            Console.WriteLine("TXT file is created...");
        }

        protected override void Open()
        {
            Console.WriteLine("TXT file is opened...");
        }

        protected override void Save()
        {
            Console.WriteLine("TXT file is saved...");
        }
    }
}

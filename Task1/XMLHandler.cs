﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class XMLHandler : AbstractHandler
    {
        public XMLHandler()
        {
            Create();
            Chenge();
            Save();
            Open();
        }
        protected override void Chenge()
        {
            Console.WriteLine("XML file is chanched...");
        }

        protected override void Create()
        {
            Console.WriteLine("XML file is created...");
        }

        protected override void Open()
        {
            Console.WriteLine("XML file is opened...");
        }

        protected override void Save()
        {
            Console.WriteLine("XML file is saved...");
        }
    }
}

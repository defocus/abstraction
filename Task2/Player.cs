﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Player : IRecordable, IPlayable
    {
        public void Play()
        {
            Console.WriteLine("Play music...");
        }

        void IPlayable.Pause()
        {
            Console.WriteLine("Pause music...");
        }

        void IPlayable.Stop()
        {
            Console.WriteLine("Stop music...");
        }

        public void Record()
        {
            Console.WriteLine("Start record...");
        }

        void IRecordable.Pause()
        {
            Console.WriteLine("Pause record...");
        }

        void IRecordable.Stop()
        {
            Console.WriteLine("Stop record...");
        }
    }
}

﻿using System;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();

            Playable(player);

            Console.WriteLine($"\n{new string('-',30)}\n");

            Recordable(player);            
        }

        static void Playable(IPlayable play)
        {
            play.Play();
            play.Pause();
            play.Stop();
        }

        static void Recordable(IRecordable record)
        {
            record.Record();
            record.Pause();
            record.Stop();
        }
    }
}
